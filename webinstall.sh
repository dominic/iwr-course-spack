#!/bin/bash

#
# This script is intended to be fetched from the web with wget and
# then piped into bash:
#
# wget -q -O - https://gitlab.dune-project.org/dominic/iwr-course-spack/-/raw/master/webinstall.sh | /bin/bash
#
# This will set up the entire course. The only prerequisite is:
# sudo apt install build-essential wget python3 git
#
# The course material will be placed inside an 'iwr-course' subfolder
# of the current directory.
#
# Before using, you should do the following in the iwr-course directory:
#
# source ./spack/share/spack/setup-env.sh
# spack activate -d ./spack
#
# Alternatively, you can write this (with the full path of your iwr-course
# directory instead of .) into ~/.bashrc.
#

# Exit on errors
set -e

# Set up the course root directory
mkdir -p iwr-course
cd iwr-course
COURSE_ROOT="$(pwd)"

# Set up a directory 'spack' that contains internals of the build process,
# I do not think this should be exposed in the top-level directory
mkdir -p spack
pushd spack

# Get Spack from Github
git clone https://github.com/spack/spack.git
source ./spack/share/spack/setup-env.sh

# Get the Dune Spack repository
git clone https://gitlab.dune-project.org/spack/dune-spack.git
spack repo add dune-spack

# Get the spack.yaml file
wget https://gitlab.dune-project.org/dominic/iwr-course-spack/-/raw/master/spack.yaml

# Create a Spack environment and install software into it
# The software list to install is specified in spack.yaml
spack env create -d $COURSE_ROOT/spack
spack env activate $COURSE_ROOT/spack
spack concretize
spack install

popd

# Get the actual course material
git clone -b master https://gitlab.dune-project.org/pdelab/dune-pdelab-tutorials.git

# Get a build directory for the course material
mkdir build
cd build
cmake ../dune-pdelab-tutorials

# Print a message about activation
echo ""
echo "The installation of Dune was successful. In order to activate the Dune"
echo "Dune environment in a shell session, you should run the following two"
echo "commands from the iwr-course directory:"
echo ""
echo "  source ./spack/share/spack/setup-env.sh"
echo "  spack env activate ./spack"
echo ""
echo "Alternatively, you can write this (with the full path of your iwr-course"
echo "directory instead of .) into ~/.bashrc."
