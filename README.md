# Spack-based installer for the IWR course

This project delivers an installer script for the IWR Dune course.
It builds the dune-pdelab-tutorials course material and all of its
dependencies. It's design aims are:

* Robustness with respect to the Linux distribution we are executing on
* A minimal amout of `sudo` operations necessary

It is based on Spack, the package manager.

# Prerequisites

The following software components are prerequisites for this installer:

* A C++ compiler
* A Python 3 interpreter
* `git`
* `wget`

If you are running on a Debian or Ubuntu OS, you can install these e.g.
by running

```
sudo apt-get install build-essential python3 git wget
```

# Running the installer

Open a terminal and run this one-line command:

```
wget -q -O - https://gitlab.dune-project.org/dominic/iwr-course-spack/raw/master/webinstall.sh | /bin/bash
```

Note that this command will take several hours to complete. Furthermore,
it needs an internet connection during that time.

After completing, a directory `iwr-course` that contains the course
material will be created. It contains the sources of `dune-pdelab-tutorials`,
as well as a `build` directory with compiled binaries of the tutorials.

# Troubleshooting

If you run into any trouble with this installer, please open an issue here:
https://gitlab.dune-project.org/dominic/iwr-course-spack/issues

Please paste as much from your build log as possible into the issue.

# Known issues

Running this installer alongside another spack installation might cause you
trouble because spack caches stuff in `~/.spack`. If you are using Spack for
yourself, I assume that you can easily set up the course material by yourself.
Look at the webinstall script for pointers.

# TODO list

* A list of packages that is useful for users should be installed as well:
  * paraview
  * nano (system nano is broken with activated spack)
